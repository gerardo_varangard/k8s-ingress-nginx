#pasos para levantarlo en k8s

requisitos:
- minikube
- kubectl
- helm

1. Configurar k8s para nginx
```kubectl apply -f mandatory.yaml```

2. Agregamos ingress como addon
``` minikube addons enable ingress ```

3. Agregamos los servicios a k8s
```kubectl apply -f services/apple.yaml
   kubecrl apply -f services/banana.yaml```

4. Creamos el cluster de ingress
```kubectl create -f ingress.yaml```

5.-verificar que este corriendo correctamente
```
curl -KL http://<ip-k8s>/apple
curl -KL http://<ip-k8s>/banana
``

- si no sabemos la ip, podemos correr:
``` minikube ip ```

